import React from 'react';
import PropTypes from 'prop-types';
import Button from "../Button";
import './Modal.scss';


export default class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.onClose = this.onClose.bind(this);
        this.onCloseArea = this.onCloseArea.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
    }

    onClose() {
        this.props.onClose()
    }

    onCloseArea(e) {
        if (e.target.className === 'modal') {
            this.onClose()
        }
    }
    onSuccess(e){
        this.props.onSuccess(e);
        this.onClose();
    }

    render() {
        let displayModal = {
            display: this.props.show ? 'flex' : 'none'
        }

        return (
            <div className='modal' style={displayModal} onClick={this.onCloseArea}>
                <div className='modal-content'>
                    <div className='modal-header'>
                        <h4>Are you sure you want to buy this?</h4>
                        <div className='button-container'>
                            <Button modification='arrow' onClick={this.onClose} text='&times;'/>
                        </div>
                    </div>
                    <div className='modal-body'>
                        <p>Add to cart</p>
                    </div>
                    <div className='modal-footer'>
                        <Button modification='action' toCart={this.props.product} onClick={this.onSuccess} text='Ok'/>
                        <Button modification='action' onClick={this.onClose} text='Cancel'/>
                    </div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    onClose: PropTypes.func,
    onSuccess: PropTypes.func,
    show: PropTypes.bool
}
